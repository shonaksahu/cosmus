from django.db import models

class Employee(models.Model):
    name = models.CharField(max_length=120)
    email = models.EmailField(unique=True)

class Event(models.Model):
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    event_type = models.CharField(max_length=55)
    event_date = models.DateField()
    