from django.contrib import admin
from .models import Employee, Event
# Register your models here.

admin.site.register(Employee)
admin.site.register(Event)